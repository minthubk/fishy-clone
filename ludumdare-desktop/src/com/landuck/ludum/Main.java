package com.landuck.ludum;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "ludumdare";
		cfg.useGL20 = true;
		cfg.vSyncEnabled = true;
		cfg.resizable = false;
		cfg.width = 960;
		cfg.height = 640;
		
		new LwjglApplication(new Core(), cfg);
	}
}
