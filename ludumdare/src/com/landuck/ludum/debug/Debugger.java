/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.debug;

public class Debugger {

	public static boolean isDebug = true;

	public static void log(String message) {
		if (isDebug)
			System.out.println("[log] " + message);
	}

	public static void setDebug(boolean debug) {
		isDebug = debug;
	}

}
