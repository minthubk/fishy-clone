/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.game;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.landuck.ludum.debug.Debugger;

public class FishCollision implements ContactListener {

	public FishCollision() {

	}

	@Override
	public void beginContact(Contact contact) {

		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();

		if (a.getBody().getUserData() != null && b.getBody().getUserData() != null) {
			if (a.getBody().getUserData().equals("player")) {

				Debugger.log("Collided");
			}
		}

	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
