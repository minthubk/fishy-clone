/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.entities;

import aurelienribon.bodyeditor.BodyEditorLoader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.landuck.ludum.Core;

public class Player extends Entity {
	
	private Vector2 origin;
	private Touchpad pad;
	private float speed = 25.0f;
	
	private boolean faceRight = true;

	public Player(World world, Touchpad pad) {

		BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/game/entities.json"));
		
		texture = Core.manager.get("data/game/player.png");
		
		sprite = new Sprite(texture);
		sprite.setColor(Color.WHITE);
	//	sprite.setScale(scale);
		
		
		BodyDef bd = new BodyDef();
		bd.type = BodyDef.BodyType.DynamicBody;
		bd.position.set(3, 3);
		
		FixtureDef d = new FixtureDef();
		
		
		body = world.createBody(bd);
		body.setFixedRotation(true);
		body.setLinearDamping(1.0f);
		body.setUserData("player");

		loader.attachFixture(body, "player", d, 102 );
		origin = loader.getOrigin("player", 102 ).cpy();

		this.pad = pad;
	}

	@Override
	public void updateAndRender(SpriteBatch batch, float delta) {

		Vector2 pos = body.getPosition().sub(origin);
		
		if(pad.getKnobPercentY() > 0) {
			body.applyLinearImpulse(new Vector2(0.0f, speed),new Vector2(sprite.getWidth() / Core.UNIT_SCALE, sprite.getHeight() / Core.UNIT_SCALE));
		}
		if(pad.getKnobPercentY() < 0) {
			body.applyLinearImpulse(new Vector2(0.0f, -speed),new Vector2(sprite.getWidth() / Core.UNIT_SCALE, sprite.getHeight() / Core.UNIT_SCALE));
		}
		
		if(pad.getKnobPercentX() > 0) {
			if(!faceRight) {
				sprite.flip(true, false);
				body.setTransform(body.getPosition(), 0 * MathUtils.degreesToRadians);
			}
			faceRight = true;
			body.applyLinearImpulse(new Vector2(speed, 0.0f),new Vector2(sprite.getWidth() / Core.UNIT_SCALE, sprite.getHeight() / Core.UNIT_SCALE));
		}
		
		if(pad.getKnobPercentX() < 0) {
			if(faceRight) {
				sprite.flip(true, false);
				body.setTransform(body.getPosition(), 180 * MathUtils.degreesToRadians);
			}
			faceRight = false;
			
			body.applyLinearImpulse(new Vector2(-speed, 0.0f),new Vector2(sprite.getWidth() / Core.UNIT_SCALE, sprite.getHeight() / Core.UNIT_SCALE));
		}
		
		sprite.setPosition(pos.x, pos.y);
		sprite.setOrigin(origin.x, origin.y);
		
		batch.begin();
			sprite.draw(batch);
		batch.end();
	}

}
