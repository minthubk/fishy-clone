/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class Entity {
	
	public Sprite sprite;
	public Texture texture;
	public float scale = 1.0f;
	public Body body;
	public Color color = new Color(Color.WHITE);
	
//	public Vector2 position = new Vector2();
	public Vector2 velocity = new Vector2();
	
	public Entity() {
		//position.x = 6.0f;
		//position.y = 0.0f;
	}
	
	public abstract void updateAndRender(SpriteBatch batch, float delta);

}
