/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class EnemyFactory {

	private World world;

	public EnemyFactory(World world) {
		this.world = world;

	}

	public Enemy getEnemy() {
		Enemy e;

		switch (getNumber(1, 4)) {

		case 1:
			e = new Enemy(new Vector2(0, getNumber(0, 9)), getNumber(0.1f, 1.0f), false, world);
			e.velocity.x = getNumber(0.2f, 2.0f);
			e.setColor(Color.YELLOW);
			e.setScale(getNumber(0.1f, 0.6f));
			break;
			
		case 2:
			e = new Enemy(new Vector2(0, getNumber(0, 9)), getNumber(0.1f, 1.0f), true, world);
			e.velocity.x = getNumber(0.2f, 2.0f);
			e.setScale(getNumber(0.1f, 0.6f));
			e.setColor(Color.GREEN);
			break;
			
		case 3:
			e = new Enemy(new Vector2(0, getNumber(0, 9)), getNumber(0.1f, 1.0f), false, world);
			e.velocity.x = getNumber(0.2f, 2.0f);
			e.setScale(getNumber(0.1f, 0.6f));
			e.setColor(Color.RED);
			break;
			
		default:
			e = new Enemy(new Vector2(0, getNumber(0, 9)), getNumber(0.1f, 1.0f), true, world);
			e.velocity.x = getNumber(0.2f, 2.0f);
			e.setScale(getNumber(0.1f, 0.6f));
			e.setColor(Color.PINK);
			
			break;
		}

		return e;
	}

	public int getNumber(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}

	public float getNumber(float min, float max) {
		return min + (float) (Math.random() * ((max - min) + 1.0f));
	}

}
