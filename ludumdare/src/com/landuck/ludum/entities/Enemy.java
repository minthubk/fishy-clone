/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.entities;

import aurelienribon.bodyeditor.BodyEditorLoader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.landuck.ludum.Core;

public class Enemy extends Entity {
	
	public boolean onScreen = true;
	public boolean faceRight = true;
	private Vector2 origin;
	Vector2 pos = new Vector2();
	BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/game/entities.json"));
	

	public Enemy(Vector2 pos, float scale, boolean right, World world) {
	
	
		texture = Core.manager.get("data/game/enemy.png");
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		sprite = new Sprite(texture);
		sprite.setScale(scale);
		sprite.setColor(color);
		
		BodyDef bd = new BodyDef();
		bd.type = BodyDef.BodyType.DynamicBody;
		if(!right) {
			bd.position.set(16 * Core.UNIT_SCALE, pos.y * Core.UNIT_SCALE);
		} else {
			bd.position.set(-2 * Core.UNIT_SCALE, pos.y * Core.UNIT_SCALE);
		}
	
		
		FixtureDef d = new FixtureDef();
		
		body = world.createBody(bd);
		body.setFixedRotation(true);
		body.setUserData("enemy");
		
		loader.attachFixture(body, "enemy1", d, 119);
		origin = loader.getOrigin("enemy1", 119 ).cpy();


		if(!right) {
			sprite.flip(true, false);
			body.setTransform(body.getPosition(), 180 * MathUtils.degreesToRadians);
		}
		this.faceRight = right;
	}

	@Override
	public void updateAndRender(SpriteBatch batch, float delta) {
	
		pos = body.getPosition().sub(origin);

		if(faceRight) {
			body.applyForce(velocity, origin);

		} else {
			body.applyForce(new Vector2(-velocity.x, 0), origin);

		}
		
		//sprite.setPosition(Core.UNIT_SCALE * body.getPosition().x - sprite.getWidth() / 2, Core.UNIT_SCALE * body.getPosition().y - sprite.getHeight() /2);
		sprite.setPosition(pos.x, pos.y);
		sprite.setOrigin(origin.x, origin.y);
		
		batch.begin();
			sprite.draw(batch);
		batch.end();
	}
	
	
	public boolean isOnScreen() {
		if(faceRight) {
			if((pos.x) - sprite.getWidth() > Gdx.graphics.getWidth()) {
				onScreen = false;
				return false;
			} else {
				onScreen = true;
				return true;
			}
		} else {
			if((pos.x ) + sprite.getWidth() > Gdx.graphics.getWidth() - Gdx.graphics.getWidth()) {
				onScreen = true;
				return true;
			} else {
				onScreen = false;
				return false;
			}
		}
	}
	
	public void dispose() {
	//	body.destroyFixture()
	}
	
	public void setScale(float scale) {
		this.scale = scale;
		sprite.setScale(scale);
	}
	
	public void setColor(Color color) {
		sprite.setColor(color);
	}

}
