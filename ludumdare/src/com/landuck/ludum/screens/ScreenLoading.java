/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.landuck.ludum.Core;
import com.landuck.ludum.debug.Debugger;

public class ScreenLoading extends ScreenAbstract {
	
	private float percent;

	public ScreenLoading(Core core) {
		super(core);
	
	}

	@Override
	public void show() {
		super.show();
		
		// Load assets
		Core.manager.load("data/ui/touchBackground.png", Texture.class);		
		Core.manager.load("data/ui/touchKnob.png", Texture.class);
		Core.manager.load("data/game/player.png", Texture.class);
		
		// Level one
		Core.manager.load("data/game/background.png", Texture.class);
		Core.manager.load("data/game/enemy.png", Texture.class);
		
		//Core.manager.finishLoading();
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(Core.manager.update()) {
			if(Gdx.input.isTouched()) {
				getCore().setScreen(new ScreenGame(getCore()));
			}
		}
		
		percent = Interpolation.linear.apply(percent, Core.manager.getProgress(), 0.1f);
		
		Debugger.log("Loaded: " + percent);

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
