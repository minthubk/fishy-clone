/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.landuck.ludum.Core;

public abstract class ScreenAbstract implements Screen {

	protected final Core core;
	protected Stage stage;

	private SpriteBatch batch;
	private BitmapFont font;
	private Table table;
	private Skin skin;
	
	
	public ScreenAbstract(Core core) {
		this.core = core;
		this.stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		
		stage.act(delta);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		stage.draw();

	}
	
	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, true);
	}
	
	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}
	
	@Override
	public void dispose() {
		if(font != null)
			font.dispose();
		if(batch != null) 
			batch.dispose();
		
		Core.manager.clear();
	}
	
	public Table getTable() {
		if(table == null) {
			table = new Table(getSkin());
			table.setFillParent(true);
			stage.addActor(table);
		}
		return table;
	}
	
	public BitmapFont getFont() {
		if(font == null) {
			font = new BitmapFont();
		}
		return font;
	}
	
	public Skin getSkin() {
		if(skin == null){
			Core.manager.load("data/ui/uiskin.json", Skin.class);
			Core.manager.finishLoading();
			skin = Core.manager.get("data/ui/uiskin.json");
		}
		return skin;
	}
	
	public SpriteBatch getBatch() {
		if(batch == null) {
			batch = new SpriteBatch();
		}
		return batch;
	}
	
	public Core getCore() {
		return this.core;
	}


}
