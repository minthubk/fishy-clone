/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.screens;

import java.sql.BatchUpdateException;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.landuck.ludum.Core;
import com.landuck.ludum.actors.JoyStick;
import com.landuck.ludum.entities.Enemy;
import com.landuck.ludum.entities.EnemyFactory;
import com.landuck.ludum.entities.Player;
import com.landuck.ludum.game.FishCollision;

public class ScreenGame extends ScreenAbstract {
	
	// World
	private World world;
	private Box2DDebugRenderer debug;
	private Texture backgroundTex;
	private Image background;
	
	// Enemies
	private float time = 0.0f;
	private float freq;
	private EnemyFactory f;
	private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	private ArrayList<Enemy> remove = new ArrayList<Enemy>();
	
	// Player
	private Player player;
	private int score = 0;
	
	// UI
	private Touchpad pad;
	
	
	public ScreenGame(Core core) {
		super(core);

	}

	@Override
	public void show() {
		super.show();
		
		world = new World(new Vector2(0.0f, 0.0f), true);
		world.setContactListener(new FishCollision());
		debug = new Box2DDebugRenderer();
		debug.setDrawVelocities(true);
		debug.setDrawBodies(true);
		
		pad = new JoyStick().getPad();
		stage.addActor(getBackground());
		stage.addActor(pad);
		
		f = new EnemyFactory(world);
		
		player = new Player(world, pad);
		
		enemies.add(f.getEnemy());
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		world.step(delta, 3, 3);
		
		getBatch().setProjectionMatrix(stage.getCamera().combined);
		
		player.updateAndRender(getBatch(), delta);
		
		time += delta;
		
		if(time >= 1) {
			//enemies.add(f.getEnemy());
			time = 0;
		}
		
		for(Enemy e : enemies) {
			if(e.isOnScreen()) {
				e.updateAndRender(getBatch(), delta);
			} else {
				remove.add(e);
			}
		}
		for(Enemy e: remove) {
			enemies.remove(e);
		}
		
		
		debug.render(world, stage.getCamera().combined);	
		
		getBatch().getProjectionMatrix().setToOrtho2D(0, 0, stage.getWidth(), stage.getHeight());
		
		getBatch().begin();
			getFont().draw(getBatch(), "Score: " + score, stage.getWidth() - 150, stage.getHeight() - getFont().getLineHeight());
			getFont().draw(getBatch(), "FPS: " + Gdx.graphics.getFramesPerSecond(), 5, stage.getHeight() - getFont().getLineHeight());
			getFont().draw(getBatch(), "RMEM: " + Core.manager.getLoadedAssets(), 5, stage.getHeight() - getFont().getLineHeight() * 2);
			getFont().draw(getBatch(), "FOS: " + enemies.size(), 5, stage.getHeight() - getFont().getLineHeight() * 3);
		getBatch().end();
		
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
	
	}
	
	public Actor getBackground() {
		backgroundTex = Core.manager.get("data/game/background.png", Texture.class);
		background = new Image(backgroundTex);
		background.setFillParent(true);
		
		return background;
	}

}
