/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.landuck.ludum.Core;

public class ScreenMenu extends ScreenAbstract {

	public ScreenMenu(Core core) {
		super(core);
	
	}

	@Override
	public void show() {
		super.show();
		
		Table table = super.getTable();
		table.getColor().a = 0;
		table.addAction(Actions.fadeIn(2.0f));
		
		// Start
		TextButton start = new TextButton("Start", getSkin());
		start.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getCore().setScreen(new ScreenLoading(getCore()));
			}
		});
		
		// Exit
		TextButton exit = new TextButton("Exit", getSkin());
		exit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		
		table.row();
		table.add(start).size(250, 50).uniform().spaceBottom(10);
		table.row();
		table.add(exit).size(250, 50).uniform().spaceBottom(10);
		
	}

	@Override
	public void render(float delta) {
		super.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
