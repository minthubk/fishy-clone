/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.landuck.ludum.debug.Debugger;
import com.landuck.ludum.screens.ScreenLoading;
import com.landuck.ludum.screens.ScreenSplash;

public class Core extends Game {

	public static AssetManager manager = new AssetManager();
	public static int UNIT_SCALE = 64;

	@Override
	public void create() {
		if (Debugger.isDebug) {
			setScreen(new ScreenLoading(this));
		} else {
			setScreen(new ScreenSplash(this));
		}

	}

}
