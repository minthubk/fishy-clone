/*************************************************************************************
 * This program is free software. 
 * 
 * It comes without any warranty, to the extent permitted by applicable law. 
 * You can redistribute it and/or modify it under the terms of the 
 * Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. 
 * 
 * See http://www.wtfpl.net/ for more details. 
 **************************************************************************************/

package com.landuck.ludum.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.landuck.ludum.Core;

public class JoyStick {

	private Touchpad pad;
	private TouchpadStyle style;
	private Skin skin;
	private Drawable bg;
	private Drawable knob;

	public JoyStick() {
		skin = new Skin();
		skin.add("touchBackground", Core.manager.get("data/ui/touchBackground.png"));
		skin.add("touchKnob", Core.manager.get("data/ui/touchKnob.png"));
		style = new TouchpadStyle();
		bg = skin.getDrawable("touchBackground");
		knob = skin.getDrawable("touchKnob");
		style.background = bg;
		style.knob = knob;

		pad = new Touchpad(10, style);
		pad.setBounds(15, 15, 200, 200);
	}

	public Touchpad getPad() {
		return pad;
	}

}
